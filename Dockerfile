# Start with the amazon base image
FROM amazonlinux:latest

# File Author / Maintainer
MAINTAINER Alex Schorsch <schorsch@stanford.edu>

# Create radmind directory and set permissions
RUN mkdir -p /var/radmind && chmod +rwx /var/radmind

# Create working directory for application

# COPY radmind-1.11.1-1.el6.rf.x86_64.rpm /tmp/rpms/radmind-1.11.1-1.el6.rf.x86_64.rpm

# Install dependencies and radmind rpm
RUN yum update -y && yum install -y diffutils nfs-utils openssl libpam.so.0 && rpm -ivh "https://hyde-imaging.stanford.edu/patches/share/rpms/radmind-server-1.14.0-1.el6.x86_64.rpm"

# Configure fstab
# RUN echo "fs-2470a38d.efs.us-west-2.amazonaws.com:/ /var/radmind nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,_netdev 0 0" >> /etc/fstab

EXPOSE 6222

#ENTRYPOINT ["/usr/sbin/radmind", "-d", "-u", "027", "-w", "2"]
CMD ["/usr/sbin/radmind", "-d", "-u", "027"]
